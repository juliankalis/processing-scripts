void setup() {
  
  size(1000, 1000);
  background(255);
  
 // picasso();
  
}

void draw() {
  frameRate(2);
  picasso(1000);
}

void picasso(int size) {
  
  float first_size = random(size/4, size * 0.75);
  float x_split = random(size);
  float y_split = random(size - first_size);
  float x_left = first_size-y_split;
  float y_left = size - first_size;
 
  strokeWeight(10);
 
  fill(random(255), random(255), random(255));
  rect(0, 0, first_size, first_size);
  
  fill(random(255), random(255), random(255));
  rect(0, first_size, x_split, y_left);
  
  fill(random(255), random(255), random(255));
  rect(x_split, first_size, size, size);
  
  fill(random(255), random(255), random(255));
  rect(first_size, 0, size, y_split);
  
  fill(random(255), random(255), random(255));
  rect(first_size, y_split, size, x_left);
  
}
