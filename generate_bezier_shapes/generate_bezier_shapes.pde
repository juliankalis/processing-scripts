void setup() {

  // choose setup values  
  size(1000, 1000);
  background(255); 
  //bezier(1000); 
}

void draw() {

  frameRate(1);
/* frameRate(60); 
  noStroke();
  fill(255, 255, 255, 5); 
  rect(0,0,500,500);
*/
  fill(255);
  rect(0,0,1000,1000);
  bezier(1000);
}

void bezier(int size) {
  
  //initialize for loop
  for (int i = 0; i < 25; i++) {
    
    //set start and end values for bezier shapes
    float start_shape_1 = random(size);
    float start_shape_2 = random(size);
    float end_shape_1 = random(size);
    float end_shape_2 = random(size);  
    //set offset for circles
    float distance_disturb = random(-10, 10);
    //set random colors in a certain spectrum
    float b = random(200, 255);
    float r = random(100, 200);
    float g = random(255);
    
    //set shape options
    smooth();
    noStroke();
    fill(r, g, b, random(255)); 
    circle(start_shape_1 + distance_disturb, start_shape_2 + distance_disturb, random(100));
    
    //draw bezier vertex
    beginShape();
    
    vertex(random(size), random(size)); 
    bezierVertex(start_shape_1,start_shape_2,random(size),random(size),end_shape_1,end_shape_2);
    bezierVertex(end_shape_1,end_shape_2,random(size),random(size),start_shape_1,start_shape_2);
    
    endShape();
  }
}
